import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "demo-ng-app";

  raiseError() {
    throw new Error("Oops something went wrong");
  }
}
